<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

Postmon é uma API para consultar CEP e encomendas de maneira fácil.

Implemente uma função que recebe CEP e retorna todos os dados reltivos ao endereço correspondente.

Exemplo:

getAddressByCep('13566400') retorna:
{
	"bairro": "Vila Marina",
	"cidade": "São Carlos",
	"logradouro": "Rua Francisco Maricondi",
	"estado_info": {
	"area_km2": "248.221,996",
	"codigo_ibge": "35",
		"nome": "São Paulo"
	},
	"cep": "13566400",
	"cidade_info": {
		"area_km2": "1136,907",
		"codigo_ibge": "3548906"
	},
	"estado": "SP"
}



Documentação:
https://postmon.com.br/


*/


//não consegui testar pois meu pc teve problema e devidos as circustâncias não consegui arrumar a tempo. tive que enviar assim mesmo...

class CEP
{
    public static function getAddressByCep($cep)
    {
		$endpoint = "https://api.postmon.com.br/v1/cep/" . $cep;
		
		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $endpoint);

		$response = curl_exec($curl);

		curl_close($curl);

		echo $response;
    }
}


var_dump(CEP::getAddressByCep(19280000));