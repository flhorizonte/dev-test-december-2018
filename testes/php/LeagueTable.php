<?php
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

A classe LeagueTablr acompanha o score de cada jogador em uma liga. Depois de cada jogo, o score do jogador é salvo utilizanod a função recordResult.

O Rank de jogar na liga é calculado utilizando a seguinte lógica:

1- O jogador com a pontuação mais alta fica em primeiro lugar. O jogador com a pontuação mais baixa fica em último.
2- Se dois jogadores estiverem empatados, o jogador que jogou menos jogos é melhor posicionado.
3- Se dois jogadores estiverem empatados na pontuação e no número de jogos disputados, então o jogador que foi o primeiro na lista de jogadores é classificado mais alto.


Implemente a funação playerRank que retorna o jogador de uma posição escolhida do ranking.

Exemplo:

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);


Todos os jogadores têm a mesma pontuação. No entanto, Arnold e Chris jogaram menos jogos do que Mike, e como Chris está acima de Arnold na lista de jogadores, ele está em primeiro lugar.

Portanto, o código acima deve exibir "Chris".


*/

class LeagueTable
{
    /**
     * Criar um array do jogadores.
     * jogador => [
     *  'index', 'games_played', 'score'
     * ]
     */
	public function __construct($players)
    {
		$this->standings = array();
		foreach($players as $index => $p)
        {
			$this->standings[$p] = array
            (
                'index' => $index,
                'games_played' => 0, 
                'score' => 0
            );
        }
	}
    /**
     * Registra o score do jogador. 
     * A cada registro games_played será incrementado.
     */
	public function recordResult($player, $score)
    {
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
	}
	/**
     * int $rank : Retornar a posição do ranking
     */
	public function playerRank($rank)
    {   
        $scores = [];
        $jogadores = [];

        $count = 1;

        foreach($this->standings as $nome => $jogador) {
            $scores[$count] = $jogador['score'];
            $jogadores[$jogador['score']] = $nome;

            $count++;
        }

        //decrescente bubble sort
        //TODO adicionar regras de ranking
        foreach($this->standings as $nome => $jogador) {
            for($i = count($scores); $i >= 1; $i--) {
                if($scores[$i] > $scores[$i - 1] && ($i - 1 >= 1)) {
                    $aux = $scores[$i - 1]; 
                    $scores[$i - 1] = $scores[$i];
                    $scores[$i] = $aux;
                }
            }
        }

        //associar os jogadores
        foreach($jogadores as $ScoreForeignKey => $jogador) {
            foreach($scores as $key => $score) {
                if($ScoreForeignKey == $score) {
                    $scores[$key] = array(
                        'nome' => $jogador,
                        'score' => $score
                    );
                }
            }
        }

        return $scores[$rank]['nome'];
	}
}
      
$table = new LeagueTable(array('Mike', 'Chris', 'Arnold', 'Felipe'));

$table->recordResult('Felipe', 2);
$table->recordResult('Felipe', 2);
$table->recordResult('Felipe', 2);
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 3);
$table->recordResult('Chris', 2);
$table->recordResult('Alan', 22);


echo $table->playerRank(2);