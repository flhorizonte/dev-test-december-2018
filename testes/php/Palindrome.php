<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    public static function isPalindrome($word)
    {
        $size = count(str_split($word)) - 1;

        $inverseWord = [];
        $notInverseWord = [];
        $palindrome = 0;

        for($letra = 0; $letra < $size; $letra++)
            $notInverseWord[$letra] = $word[$letra];
        for($inverse = $size; $inverse >= 0; $inverse--)
            $inverseWord[$inverse] = $word[$inverse];
        foreach($notInverseWord as $key => $letra) 
            if($notInverseWord[$key] == $notInverseWord[$key]) 
                $palindrome++;

        if($palindrome == $size)
            return true;

        return false;
    }
}

echo Palindrome::isPalindrome('Deleveled') ? 'Is Palindrome' : 'Is not a Palindrome';