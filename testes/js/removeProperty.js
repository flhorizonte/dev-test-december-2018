// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {

  if(Object.keys(obj)[0] == prop) {

    delete obj[Object.keys(obj)[0]]
    
    return true
  }

  return false
}


removeProperty({eae: 1}, 'eae')
